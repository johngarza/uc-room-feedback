/*global Feedback, $*/


window.Feedback = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
        'use strict';
        console.log('Hello from Backbone!');
    }
};

$(document).ready(function () {
    'use strict';
    Feedback.init();
    window.Feedback.router = new Feedback.Routers.Feedback();
    window.Feedback.model = new Feedback.Models.Survey();
    window.Feedback.view = new Feedback.Views.Intro({ el: $('#app'), model: window.Feedback.model });
    Backbone.history.start();
    window.Feedback.view.render();
});
