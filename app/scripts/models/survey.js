/*global Feedback, Backbone*/

Feedback.Models = Feedback.Models || {};

(function () {
  'use strict';
  
  Feedback.Models.Survey = Backbone.Model.extend({
    initialize: function() {},
    defaults: {
      'room' : 'none',
      'ref' : 'none',
      'name' : 'none',
      'org' : 'none',
      'email' : 'none',
    },
    entryProp: {
      'room' : 'entry.378994219',
      'ref' : 'entry.1128172726',
      'name' : 'entry.2066991919',
      'org' : 'entry.433920007',
      'email' : 'entry.88703133'
    },
    embedURL: function() {
      var that = this;
      var urlStr = 'https://docs.google.com/forms/d/';
      var urlSuffix = 'entry.1566488799&entry.690140872&entry.1837095669&entry.1443087508';
      urlStr += '1nIsThT0zsK0w1NJ2Nw5NpiPpw7GddqGa1bJVgCytCT0/viewform?';
      _.forEach(_.keys(that['entryProp']), function(id) {
        urlStr += that['entryProp'][id] + '=' + that.attributes[id] + '&';
      });
      urlStr += urlSuffix;
      return urlStr;
    }
  });
})();
