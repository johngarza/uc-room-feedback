/*global Feedback, Backbone*/

Feedback.Routers = Feedback.Routers || {};

(function () {
  'use strict';
  Feedback.Routers.Feedback = Backbone.Router.extend({
    routes: {
      "":               "introRoute",
      ":ref":           "introRoute",
      "fill/:ref":      "fillSurvey"
    },
    introRoute: function(ref) {
      var survey = new Feedback.Models.Survey({
        'foo' : 'bar'
      });
      console.log("introRoute entered");
      console.log("passed ref: " + ref);
      if (ref) {
      
      } else {
      
      }
    },
    fillSurvey: function(ref) {  
    }
  });
})();
