/*global Feedback, Backbone, JST*/

Feedback.Views = Feedback.Views || {};

(function () {
    'use strict';
    Feedback.Views.Intro = Backbone.View.extend({
        template: JST['app/scripts/templates/intro.ejs'],
        tagName: 'div',
        id: '',
        className: '',
        events: {},
        initialize: function () {
            //this.listenTo(this.model, 'change', this.render);
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
        }
    });
})();
